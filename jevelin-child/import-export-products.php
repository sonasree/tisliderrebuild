<?php

/* CSV IMPORT / EXPORT Feature */

add_action('admin_menu', 'tiautomotive_theme_menu');
function tiautomotive_theme_menu() {
    add_menu_page('Products CSV', 'Import CSV', 'manage_options', 'products-options', 'tiautomotive_products_options', 'dashicons-location-alt');
}

function tiautomotive_products_options() {  ?>

    <style type="text/css">
        .products-csv-wrap {
            margin: 20px 20px 20px 0;
            padding: 20px;
            background: #fff;
        }
    </style>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" crossorigin="anonymous"></script>

    <div class="products-csv-wrap">
        <h3>Import CSV data</h3>
        <form id="formImportCSV" class="form-horizontal" action="#" method="post" name="upload_excel" enctype="multipart/form-data">
            <fieldset>
                <!-- Form Name -->
                <legend></legend>
                <!-- File Button -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="filebutton">Select File</label>
                    <div class="col-md-4">
                        <input type="file" name="file" id="file" class="input-large" required accept=".csv">
                    </div>
                </div>
                <!-- Button -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="singlebutton">Import data</label>
                    <div class="col-md-4">
                        <button type="submit" id="submit" name="Import" class="btn btn-primary button-loading" data-loading-text="Loading...">Import</button>
                    </div>
                </div>

            </fieldset>
        </form>
        <script type="text/javascript">
          jQuery(document).ready(function () {

              jQuery("#formImportCSV :file").on("change", function(e) {
                //console.log(this.files[0].type);
                if( this.files[0].name.toLowerCase().lastIndexOf(".csv") == -1 ) {
                  alert('Invalid File:Please Upload CSV File.');
                  jQuery("form#formImportCSV").get(0).reset();
                }
                
              });              

          });
        </script>
    </div>
    <?php

    if(isset($_POST["Import"])){
            
        $filename=$_FILES["file"]["tmp_name"];    

        if($_FILES["file"]["size"] > 0) {      

            $file = fopen($filename, "r");

            if($file) {

                global $wpdb;
                $uhp_products = array(
                    "custom-field-horsepower" => "horsepower",
                    "custom-field-flow_rate" => "flow_rate",
                    "custom-field-max_pressure" => "max_pressure",
                    "custom-field-fuel_type" => "fuel_type",
                    "custom-field-pump_type" => "pump_type",
                    "custom-field-pump_body_size" => "pump_body_size",
                    "custom-field-internal_check_valve" => "internal_check_valve",
                    "custom-field-recommended_for_boosted_applications" => "recommended_for_boosted_applications",
                    "custom-field-notes" => "notes",
                    "custom-field-related_products" => "related_products",
                    "custom-post-type" => "product",
                    "custom-post-product_category" => 54
                );

                // Delete all universal high performance products
                // $wpdb->delete( $wpdb->posts,  array(
                //     'post_type' => $uhp_products["custom-post-type"],
                //     'tax_query' => array(
                //         array(
                //             'taxonomy' => 'product_cat',
                //             'field' => 'term_id',
                //             'terms' => $uhp_products["custom-post-product_category"],
                //         )
                //     )
                // ) );

                $all_uhp_products = get_posts( array(
                    'post_type' => $uhp_products["custom-post-type"],
                    'numberposts'=> -1,
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'product_cat',
                            'field' => 'term_id',
                            'terms' => $uhp_products["custom-post-product_category"],
                        )
                    )
                ) );
 
                foreach ( $all_uhp_products as $single_uhp_product ) {
                    wp_delete_post( $single_uhp_product->ID, true); // Set to False if you want to send them to Trash.
                } 

                $result = false;

                while (($getData = fgetcsv($file, 10000, ",")) !== FALSE) {    
        
                    if($getData[0] != 'Part number') {
                        
                        // Insert the post into the database
                        $post["id"] = wp_insert_post( array(
                            "post_title" => $getData[0],
                            "post_type" => $uhp_products["custom-post-type"],
                            "post_content" => $getData[7],
                            "post_status" => "publish"
                        ));
                        
                        //$product_cat = get_term_by('slug', 'universal-high-performance', 'product_cat');
                        wp_set_post_terms( $post["id"], $uhp_products["custom-post-product_category"], 'product_cat', true );
                    
                        // Update post's custom fields
                        update_field( $uhp_products["custom-field-flow_rate"], numberTrim($getData[1]), $post["id"] );
                        update_field( $uhp_products["custom-field-max_pressure"], numberTrim($getData[2]), $post["id"] );
                        update_field( $uhp_products["custom-field-horsepower"], numberTrim($getData[3]), $post["id"] );
                        update_field( $uhp_products["custom-field-fuel_type"], strtoupper($getData[4]), $post["id"] );
                        update_field( $uhp_products["custom-field-pump_type"], $getData[5], $post["id"] );
                        update_field( $uhp_products["custom-field-pump_body_size"], $getData[6], $post["id"] );
                        update_field( $uhp_products["custom-field-internal_check_valve"], $getData[8], $post["id"] );
                        update_field( $uhp_products["custom-field-recommended_for_boosted_applications"], $getData[9], $post["id"] );
                        update_field( $uhp_products["custom-field-notes"], $getData[10], $post["id"] );
                        
                        $result = $post["id"];
                    }
                    
                }

            }
            
            fclose($file); 
            echo "<script type=\"text/javascript\">
                    alert(\"CSV File has been successfully Imported.\");
                    window.location = \"edit.php?post_type=product\"
                </script>";
        }
    }
} 


/**
 * Show 'insert posts' button on backend
 */
add_action( "admin_notices", function() {
    echo "<div class='updated'>";
    echo "<p>";
    echo "Click <a href='//{$_SERVER['SERVER_NAME']}/wp-admin/admin.php?page=products-options'>here</a> to import Universal High Performance products.";
    echo "</p>";
    echo "</div>";
});

function numberTrim($mixedstring) {
    $numberonly = preg_replace("/[^0-9]/", "", $mixedstring );
    return $numberonly;
}

?>