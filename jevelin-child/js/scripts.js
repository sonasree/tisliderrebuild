(function detectIE() {
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf('MSIE ');
    var trident = ua.indexOf('Trident/');
    var edge = ua.indexOf('Edge/');
    if (msie > 0) {
        $("body").addClass("ie")
    }
    else if (trident > 0) {
      $("body").addClass("ie ie-11")
    }
    else if (edge > 0) {
        $("body").addClass("ie-edge")
    }
    return false;
  })();

jQuery(document).ready(function ($) {
    function setValue(_val, meter) {
        var START, delta;
        var mainPoint = Math.floor(_val/100);
        var smallPoint = Math.round(_val/100);
        var i=1;
        switch (meter) {
            case "horsepower-meter":
                START = 0;
                delta = 0.0818
                $('.horsepower-meter .mainPoints, .horsepower-meter .midPoints').find('path').removeClass('active');
                while (i <= mainPoint+1) {
                    $('.horsepower-meter .mainPoints').find('path:nth-child('+ i +')').addClass('active');
                    // for small point correction
                    if(isNaN(false)) {
                        if(smallPoint > mainPoint) {
                            $('.horsepower-meter .midPoints').find('path:nth-child('+ (i) +')').addClass('active');
                        }
                        else {
                            $('.horsepower-meter .midPoints').find('path:nth-child('+ (i - 1) +')').addClass('active');
                        }
                    }
                    i++;
                }

                // For Current marking
                $('.horsepower-meter .mainPoints, .horsepower-meter .midPoints').find('path').removeClass('current');
                // if(isNaN(false)) {
                    if(smallPoint > mainPoint) {
                        $('.horsepower-meter .midPoints').find('path:nth-child('+ (mainPoint+1) +')').addClass('current');
                    }
                    else {
                        $('.horsepower-meter .mainPoints').find('path:nth-child('+ (mainPoint+1) +')').addClass('current');
                    }
                // }
                break;
            case "flow-rate-meter":
                START = 0;
                delta = 0.218
                $('.flow-rate-meter .mainPoints, .flow-rate-meter .midPoints').find('path').removeClass('active');
                while (i <= mainPoint+1) {
                    $('.flow-rate-meter .mainPoints').find('path:nth-child('+ i +')').addClass('active');
                    // for small point correction
                    if(isNaN(false)) {
                        if(smallPoint > mainPoint){
                            $('.flow-rate-meter .midPoints').find('path:nth-child('+ (i) +')').addClass('active');
                        }
                        else {
                            $('.flow-rate-meter .midPoints').find('path:nth-child('+ (i - 1) +')').addClass('active');
                        }
                    }
                    i++;
                }

                // For Current marking
                $('.flow-rate-meter .mainPoints, .flow-rate-meter .midPoints').find('path').removeClass('current');
                if(isNaN(false)){
                    if(smallPoint > mainPoint) {
                        $('.flow-rate-meter .midPoints').find('path:nth-child('+ (mainPoint+1) +')').addClass('current');
                    }
                    else {
                        $('.flow-rate-meter .mainPoints').find('path:nth-child('+ (mainPoint+1) +')').addClass('current');
                    }
                }
                break;
            case "max-pressure-meter":
                START = 0;
                delta = 1.15
                $('.max-pressure-meter .mainPoints, .max-pressure-meter .midPoints').find('path').removeClass('active');
                while (i <= mainPoint+1) {
                    $('.max-pressure-meter .mainPoints').find('path:nth-child('+ i +')').addClass('active');
                    // for small point correction
                    if(isNaN(false)){
                        if(smallPoint > mainPoint) {
                            $('.max-pressure-meter .midPoints').find('path:nth-child('+ (i) +')').addClass('active');
                        }
                        else {
                            $('.max-pressure-meter .midPoints').find('path:nth-child('+ (i - 1) +')').addClass('active');
                        }
                    }
                    i++;
                }

                // For Current marking
                $('.max-pressure-meter .mainPoints, .max-pressure-meter .midPoints').find('path').removeClass('current');
                if(isNaN(false)){
                    if(smallPoint > mainPoint) {
                        $('.max-pressure-meter .midPoints').find('path:nth-child('+ (mainPoint+1) +')').addClass('current');
                    }
                    else {
                        $('.max-pressure-meter .mainPoints').find('path:nth-child('+ (mainPoint+1) +')').addClass('current');
                    }
                }
                break;
        }

        $('.' + meter).find('.counter, .counter-ie').text(_val);
        deg = START + _val * delta;
        setTimeout(() => {
            $('.' + meter).find('.arrow').css({
                "transform": 'rotate(' + deg + 'deg)'
            });
        }, 100);
        
        switch ($('.' + meter).find('.counter').text().length) {
            case 1:
                if (meter != "horsepower-meter") {
                    $('.' + meter).find('.counter').attr('x', '-6%');
                } else {
                    $('.' + meter).find('.counter').attr('x', '-5%');
                }
                break;
            case 2:
                if (meter != "horsepower-meter") {
                    $('.' + meter).find('.counter').attr('x', '-13%');
                } else {
                    $('.' + meter).find('.counter').attr('x', '-9%');
                }
                break;
            case 3:
                if (meter != "horsepower-meter") {
                    $('.' + meter).find('.counter').attr('x', '-20%');
                } else {
                    $('.' + meter).find('.counter').attr('x', '-13%');
                }
                break;
            case 4:
                $('.' + meter).find('.counter').attr('x', '-18%');
                break;
        }
    };

    // For Available values
    function availables(filter) {
        $('.ruler-value span').removeClass('avl');
        $.each(filter['horsepower'], function( index, value ) { 
            $('.range-inner--horsepower .ruler-value [data-value="'+value+'"]').addClass('avl');
        });
        $.each(filter['flow_rate'], function( index, value ) { 
            $('.range-inner--flow-rate .ruler-value [data-value="'+value+'"]').addClass('avl');
        });
        $.each(filter['max_pressure'], function( index, value ) { 
            $('.range-inner--max-pressure .ruler-value [data-value="'+value+'"]').addClass('avl');
        });
        if(filter['fuel_type'][0] == "GAS") {
            $('.range-inner--fuel-type .ruler-value [data-value="GAS"]').addClass('avl');
        }
        else if(filter['fuel_type'][0] == "FLEX") {
            $('.range-inner--fuel-type .ruler-value [data-value="FLEX"]').addClass('avl');
        }
    }
    $('.range-inner .value').on('input', function(e) {
        e.stopPropagation();
        var fltype = "";
        if($(this).attr('id') == "fuel-type_value") {
            if ($('#fuel-type_value').val() == "gas") {
                fltype = "GAS";
            } 
            else if ($('#fuel-type_value').val() == "flex") {
                fltype = "FLEX";
            }
            else if ($('#fuel-type_value').val() == "") {
                fltype = "";
            }
        }
    
        $('#filterTrobber').show();
        setTimeout(function(){
            var filters = checkAvailableFilters($('#horsepower_value').val(),$('#flow-rate_value').val(),$('#max-pressure_value').val(),fltype);
            availables(filters);
        },100);
    });

    function initiateMeter(range, sliderId, meter) {
        var ua = window.navigator.userAgent;
        var msie = ua.indexOf('MSIE ');
        var trident = ua.indexOf('Trident/');
        var edge = ua.indexOf('Edge/');
        if (msie > 0 || trident > 0) {
            $('#' + sliderId).on('input change', function (e) {
                e.stopPropagation();
                var min = e.target.min,
                    max = e.target.max,
                    val = e.target.value;
                    ruler = $("#" + sliderId).find('.ruler-value span');
                $(e.target).css({
                    'backgroundSize': (val - min) * 100 / (max - min) + '% 100%'
                });

                $(this).closest(".range-inner").find(".value, .range-counter").val(range[this.value]).text((range[this.value])).trigger('input');
                if (val != 0) {
                    // $(this).closest('.range-inner').find('.ruler-value').addClass('active');
                    $('.range-inner').find('.ruler-value').addClass('active');
                    $(this).closest('.range-inner').find('.ruler-value span').removeClass('active');
                    $(this).closest('.range-inner').find('.ruler-value span:nth-child(' + (val) + ')').addClass('active');
                } else {
                    $(this).closest('.range-inner').find('.ruler-value').removeClass('active');
                    $(this).closest('.range-inner').find('.ruler-value span').removeClass('active');
                }
                setValue(range[this.value], meter);

                if(meter == 'fuel-type-meter')
                {
                    if (val == min) {
                        $(this).closest(".toggle-wrapper").removeClass("max").addClass("min");
                        $(".fuel-meter").removeClass("max").addClass("min");
                        $(this).closest(".range-inner").find(".value").val("gas").trigger('input');
                    } else if (val == max) {
                        $(this).closest(".toggle-wrapper").removeClass("min").addClass("max");
                        $(".fuel-meter").removeClass("min").addClass("max");
                        $(this).closest(".range-inner").find(".value").val("flex").trigger('input');
                    } else {
                        $(this).closest(".toggle-wrapper").removeClass("min max");
                        $(".fuel-meter").removeClass("min max");
                        $(this).closest(".range-inner").find(".value").val("").trigger('input');
                    }
                }
            }).trigger('input');
        }
        else {
            $('#' + sliderId).on('input', function (e) {
                e.stopPropagation();
                var min = e.target.min,
                    max = e.target.max,
                    val = e.target.value;
                    ruler = $("#" + sliderId).find('.ruler-value span');
                $(e.target).css({
                    'backgroundSize': (val - min) * 100 / (max - min) + '% 100%'
                });

                $(this).closest(".range-inner").find(".value, .range-counter").val(range[this.value]).text((range[this.value])).trigger('input');
                if (val != 0) {
                    // $(this).closest('.range-inner').find('.ruler-value').addClass('active');
                    $('.range-inner').find('.ruler-value').addClass('active');
                    $(this).closest('.range-inner').find('.ruler-value span').removeClass('active');
                    $(this).closest('.range-inner').find('.ruler-value span:nth-child(' + (val) + ')').addClass('active');
                    var current = $(this).closest('.range');
                    setTimeout(function(){
                    var count = $(current).next().find('.range-inner .ruler-value span.avl').first().data("count");
                    var value = $(current).next().find('.range-inner .ruler-value span.avl').first().data("value")
                    $(current).next().find('.range-input').val(count);
                    $(current).next().find(".range-inner").find(".value, .range-counter").val(value).text(value);
                    $(current).next().find(".range-input").trigger('input');
                    }, 2500);
                    $('.range-inner--horsepower .ruler-value span').addClass('avl');
                } else {
                    $(this).closest('.range-inner').find('.ruler-value').removeClass('active');
                    $(this).closest('.range-inner').find('.ruler-value span').removeClass('active');
                }
                setValue(range[this.value], meter);

                if(meter == 'fuel-type-meter')
                {
                    if (val == min) {
                        $(this).closest(".toggle-wrapper").removeClass("max").addClass("min");
                        $(".fuel-meter").removeClass("max").addClass("min");
                        $(this).closest(".range-inner").find(".value").val("gas").trigger('input');
                    } else if (val == max) {
                        $(this).closest(".toggle-wrapper").removeClass("min").addClass("max");
                        $(".fuel-meter").removeClass("min").addClass("max");
                        $(this).closest(".range-inner").find(".value").val("flex").trigger('input');
                    } else {
                        $(this).closest(".toggle-wrapper").removeClass("min max");
                        $(".fuel-meter").removeClass("min max");
                        $(this).closest(".range-inner").find(".value").val("").trigger('input');
                    }
                }
            }).trigger('input');
        }
    }

    var hp_values = [0, 300, 350, 400, 420, 450, 500, 550, 650, 850, 985, 1000, 1250, 1450, 1600];
    var fr_values = [0, 190, 255, 340, 350, 400, 450, 470, 490, 500, 550, 725, 775];
    var mp_values = [0, 50, 87, 112];
    var fuel_values = [0, 1, 2];

    initiateMeter(hp_values, "horsepower", "horsepower-meter"); 
    initiateMeter(fr_values, "flow-rate", "flow-rate-meter"); 
    initiateMeter(mp_values, "max-pressure", "max-pressure-meter");   
    initiateMeter(fuel_values, "fuel-type", "fuel-type-meter");  

    $("#reset").on("click", function (e) {
        e.preventDefault();
        $("input[type='range']").val(0).css({
            'backgroundSize': 0
        });
        $(".arrow").css({
            "transform": 'rotate(' + 0 + 'deg)'
        });
        $(".horsepower-meter .counter").attr('x', '-5%');
        $(".flow-rate-meter .counter, .max-pressure-meter .counter").attr('x', '-6%');
        $(".midPoints, .mainPoints").find('path').removeClass('active');
        $(".ruler-value").removeClass('active');
        $(".ruler-value span").removeClass('avl active');
        $(".counter, .counter-ie, .value, .range-counter").text(0).val(0);
        $(".toggle-wrapper, .fuel-meter").removeClass("min max");
    });

    $(".results-btn").on("click", function () {
        var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
        if (isMobile) {
            $('html, body').animate({
                scrollTop: $(".filter-content").offset().top - $(".sh-header-mobile").height()
            }, 1000);
        }
        else {
            $('html, body').animate({
                scrollTop: $(".filter-content").offset().top - $(".sh-header").height()
            }, 1000);
        }

    });

    $(".modify-search-btn").on("click", function () {
        $('html, body').animate({
            scrollTop: $('body').offset().top
        }, 1000);
    });
    
    $('.title-wrapper .modal-toggle').on('click', function(e) {
        e.preventDefault();
        $('.title-wrapper .modal').toggleClass('is-visible');
    });
    
    $('body').click(function(e) {
        if (!$(e.target).closest('.title-wrapper .modal, .title-wrapper .modal-toggle').length) {
            $(".title-wrapper .modal").removeClass('is-visible');
        }
    });

    // Fetch 
    var url_string = window.location.href
    function fetchFromUrl() {
        var url = new URL(url_string);
        var horsepower = url.searchParams.get("horsepower");
        var flow_rate = url.searchParams.get("flow_rate");
        var max_pressure = url.searchParams.get("max_pressure");
        var fuel_type = url.searchParams.get("fuel_type");
        function setTheValue(meter, _value) {
            switch (meter) {
                case "horsepower":
                    setValue(_value, meter);
                    $('#'+meter).val(hp_values.indexOf(parseInt(_value))).trigger('input');
                    break;
                case "flow-rate":
                    setValue(_value, meter);
                    $('#'+meter).val(fr_values.indexOf(parseInt(_value))).trigger('input');
                    break;
                case "max-pressure":
                    setValue(_value, meter);
                    $('#'+meter).val(mp_values.indexOf(parseInt(_value))).trigger('input');
                    break;
                case "fuel-type":
                    setValue(_value, meter);
                    $('#'+meter).val(_value).trigger('input');
                    if(_value == "GAS") {
                        $('#'+meter).val(0);
                        $('#'+meter).closest(".toggle-wrapper").removeClass("max").addClass("min");
                        $(".fuel-meter").removeClass("max").addClass("min");
                        $('#'+meter).closest(".range-inner").find(".value").val("gas");
                    }
                    else if(_value == "FLEX") {
                        $('#'+meter).val(2);
                        $('#'+meter).closest(".toggle-wrapper").removeClass("min").addClass("max");
                        $(".fuel-meter").removeClass("min").addClass("max");
                        $('#'+meter).closest(".range-inner").find(".value").val("flex");
                    }
                    else {
                        $('#'+meter).closest(".toggle-wrapper").removeClass("min max");
                        $(".fuel-meter").removeClass("min max");
                        $('#'+meter).closest(".range-inner").find(".value").val("");
                    }
                    break;
            }
        } 
        setTheValue("horsepower", horsepower);
        setTheValue("flow-rate", flow_rate);
        setTheValue("max-pressure", max_pressure);
        setTheValue("fuel-type", fuel_type);
    }
    if(url_string.includes('?')) {
        fetchFromUrl();
    }
   
});

jQuery(window).scroll(function () {
    var hH = jQuery('.section-landing').outerHeight() + 100;
    var wS = jQuery(this).scrollTop();
    if (wS > hH) {
        jQuery(".modify-search-btn").addClass("show")
    } else {
        jQuery(".modify-search-btn").removeClass("show")
    }
});