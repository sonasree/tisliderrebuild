jQuery(document).ready(function ($) {
        
    // Internet Explorer 6-11
    var isIE = /*@cc_on!@*/false || !!document.documentMode;
    if(isIE) {
        $('body').addClass('ie');
    }

    // Value range for sliders
    var hp_values = [0, 300, 350, 400, 420, 450, 500, 550, 650, 850, 985, 1000, 1250, 1450, 1600];
    var fr_values = [0, 190, 255, 340, 350, 400, 450, 470, 490, 500, 550, 725, 775];
    var mp_values = [0, 50, 87, 112];

    // Horsepower
    $('#horsepower').on('change', function (e) {
        e.stopPropagation();
        
        var min = e.target.min,
            max = e.target.max,
            val = e.target.value;
            ruler = $("horsepower").find('.ruler-value span');

        // set backgroud for slider
        $(e.target).css({
            'backgroundSize': (val - min) * 100 / (max - min) + '% 100%'
        });

        $('.range-inner--horsepower').find('.ruler-value span').removeClass('active');
        $('.range-inner--horsepower').find('.ruler-value span:nth-child(' + (val) + ')').addClass('active');
        $(".range-inner--horsepower").find(".value").attr("value", hp_values[this.value]);
        $(".range-inner--horsepower").find(".value, .range-counter").val(hp_values[this.value]).text((hp_values[this.value])).trigger('change');


        setTimeout(function(){
            var filter = checkAvailableFilters($('#horsepower_value').val(),"","","");
            $('.ruler-value span').removeClass('avl');
            $.each(filter['horsepower'], function( index, value ) { 
                $('.range-inner--horsepower .ruler-value [data-value="'+value+'"]').addClass('avl');
            });
            $.each(filter['flow_rate'], function( index, value ) {
                $('.range-inner--flow-rate .ruler-value [data-value="'+value+'"]').addClass('avl');
                setTimeout(function(){
                    var count =  $('.range-inner--flow-rate .ruler-value [data-value="'+value+'"]').data("count");
                    $(".range-inner--flow-rate").find('.range-input').val(count).trigger('change');
                },100);
            });
        },100);

        // Rotate Meter
        $('.horsepower-meter').find('.counter, .counter-ie').text(hp_values[this.value]);
        var START = 0;
        var delta = 0.0818;
        var deg = START + hp_values[this.value] * delta;
        setTimeout(function(){
            $('.horsepower-meter').find('.arrow').css({
                "-webkit-transform": "rotate(" + deg + "deg)",
                "-o-transform": 'rotate(' + deg + 'deg)',
                "-moz-transform": 'rotate(' + deg + 'deg)',
                "transform": 'rotate(' + deg + 'deg)',
            });
        },1000);
    });

    // Flowrate
    $('#flow-rate').on('change', function (e) {
        e.stopPropagation();
        var min = e.target.min,
            max = e.target.max,
            val = e.target.value;
            ruler = $("flow-rate").find('.ruler-value span');

        var avlmin = $('.range-inner--flow-rate .ruler-value span.avl').first().data("count");
        var avlmax = $('.range-inner--flow-rate .ruler-value span.avl').last().data("count");

        var currentVal = this.value;

        // enable to slide for the available range only
        if(this.value>avlmax) {
            currentVal = avlmax;
        } else if(this.value<avlmin) {
            currentVal = avlmin;
        } else {
            currentVal = this.value;
        }

        // set backgroud for slider
        $(e.target).css({
            'backgroundSize': (currentVal - min) * 100 / (max - min) + '% 100%'
        });

        $('#flow-rate').val(currentVal);

        $('.range-inner--flow-rate').find('.ruler-value span').removeClass('active');
        $('.range-inner--flow-rate').find('.ruler-value span:nth-child(' + (currentVal) + ')').addClass('active');
        $(".range-inner--flow-rate").find(".value").attr("value", fr_values[currentVal]);
        $(".range-inner--flow-rate").find(".value, .range-counter").val(fr_values[currentVal]).text((fr_values[currentVal]));

        setTimeout(function(){
            var filter = checkAvailableFilters($('#horsepower_value').val(),fr_values[currentVal],"","");
            $('.range-inner--max-pressure .ruler-value span').removeClass('avl');
            $.each(filter['max_pressure'], function( index, value ) { 
                $('.range-inner--max-pressure .ruler-value [data-value="'+value+'"]').addClass('avl');
                setTimeout(function(){
                    var count =  $('.range-inner--max-pressure .ruler-value [data-value="'+value+'"]').data("count");
                    $(".range-inner--max-pressure").find('.range-input').val(count).trigger('change');
                },100);
            });
        },100);

        // Rotate Meter
        $('.flow-rate-meter').find('.counter, .counter-ie').text(fr_values[currentVal]);
        var START = 0;
        var delta = 0.218;
        var deg = START + fr_values[currentVal] * delta;
        setTimeout(function(){
            $('.flow-rate-meter').find('.arrow').css({
                "-webkit-transform": 'rotate(' + deg + 'deg)',
                "-o-transform": 'rotate(' + deg + 'deg)',
                "-moz-transform": 'rotate(' + deg + 'deg)',
                "transform": 'rotate(' + deg + 'deg)',
            });
        }, 1000);
    });

    //Max Pressure
    $('#max-pressure').on('change', function (e) {
        e.stopPropagation();
        var min = e.target.min,
            max = e.target.max,
            val = e.target.value;
            ruler = $("max-pressure").find('.ruler-value span');

        var avlmin = $('.range-inner--max-pressure .ruler-value span.avl').first().data("count");
        var avlmax = $('.range-inner--max-pressure .ruler-value span.avl').last().data("count");

        var currentVal = this.value;

        // enable to slide for the available range only
        if(this.value>avlmax) {
            currentVal = avlmax;
        } else if(this.value<avlmin) {
            currentVal = avlmin;
        } else {
            currentVal = this.value;
        }
        
        // set backgroud for slider
        $(e.target).css({
            'backgroundSize': (currentVal - min) * 100 / (max - min) + '% 100%'
        });

        $('#max-pressure').val(currentVal);

        $('.range-inner--max-pressure').find('.ruler-value span').removeClass('active');
        $('.range-inner--max-pressure').find('.ruler-value span:nth-child(' + (currentVal) + ')').addClass('active');
        $(".range-inner--max-pressure").find(".value").attr("value", mp_values[currentVal]);
        $(".range-inner--max-pressure").find(".value, .range-counter").val(mp_values[currentVal]).text((mp_values[currentVal]));

        setTimeout(function(){
            var filter = checkAvailableFilters($('#horsepower_value').val(),$('#flow-rate_value').val(),mp_values[currentVal],"");
            $('.range-inner--fuel_type .ruler-value span').removeClass('avl');
            $('.range-inner--fuel-type').find('.ruler-value span').removeClass('active');
            $('#fuel-type').val(0).trigger('change');
            if(filter['fuel_type'][0] == "GAS") {
                $('.range-inner--fuel-type .ruler-value [data-value="GAS"]').addClass('avl');
                $('#fuel-type').val(-1).trigger('change');
                $('.range-inner--fuel-type').find('.ruler-value span:first-child').addClass('active');
            }
            else if(filter['fuel_type'][0] == "FLEX") {
                $('.range-inner--fuel-type .ruler-value [data-value="FLEX"]').addClass('avl');
                $('#fuel-type').val(1).trigger('change');
                $('.range-inner--fuel-type').find('.ruler-value span:last-child').addClass('active');
            }
        },100);

        // Rotate Meter
        $('.max-pressure-meter').find('.counter, .counter-ie').text(mp_values[this.value]);
        var START = 0;
        var delta = 1.15;
        var deg = START + mp_values[this.value] * delta;
        setTimeout(function(){
            $('.max-pressure-meter').find('.arrow').css({
                "-webkit-transform": 'rotate(' + deg + 'deg)',
                "-o-transform": 'rotate(' + deg + 'deg)',
                "-moz-transform": 'rotate(' + deg + 'deg)',
                "transform": 'rotate(' + deg + 'deg)',
            });
        }, 1000);
    });

    // Fuel Type
    $('#fuel-type').on('change', function (e) {
        e.stopPropagation();
        var min = e.target.min,
            max = e.target.max,
            val = e.target.value;

        $(e.target).css({
            'backgroundSize': (val - min) * 100 / (max - min) + '% 100%'
        });

        $('.range-inner--fuel-type').find('.ruler-value span').removeClass('active');

        var fltype = "";
        if (val == -1) {
            fltype = "GAS";
            $(".range-inner--fuel-type .toggle-wrapper").removeClass("max").addClass("min");
            $('.range-inner--fuel-type').find('.ruler-value span:first-child').addClass('active');
        } 
        else if (val == 1) {
            fltype = "FLEX";
            $(".range-inner--fuel-type .toggle-wrapper").removeClass("min").addClass("max");
            $('.range-inner--fuel-type').find('.ruler-value span:last-child').addClass('active');
        }
        else if (val == 0) {
            fltype = "";
            $(".range-inner--fuel-type .toggle-wrapper").removeClass("min").removeClass("max");
        }        

        $(".range-inner--fuel-type").find(".value").attr("value", fltype);
        $(".range-inner--fuel-type").find(".value").attr("value", fltype);
        $(".range-inner--fuel-type").find(".value, .range-counter").val(fltype).text(fltype);

        // Fetch results
        searchFilterPumps();
    });

    // Reset
    $("#reset").on("click", function (e) {
        e.preventDefault();
        $("input[type='range']").val(0).css({
            'backgroundSize': 0
        });
        $(".arrow").css({
            "transform": 'rotate(' + 0 + 'deg)'
        });
        $(".horsepower-meter .counter").attr('x', '-5%');
        $(".flow-rate-meter .counter, .max-pressure-meter .counter").attr('x', '-6%');
        $(".midPoints, .mainPoints").find('path').removeClass('active');
        $(".ruler-value").removeClass('active');
        $(".ruler-value span").removeClass('avl active');
        $(".counter, .counter-ie, .value, .range-counter").text(0).val(0);
        $(".toggle-wrapper, .fuel-meter").removeClass("min max");
    });
});