jQuery(document).ready(function ($) {
    // Internet Explorer 6-11
    var isIE = /*@cc_on!@*/false || !!document.documentMode;
    if(isIE) {
        $('body').addClass('ie');
    }

    // Value range for sliders
    var hp_values = [0, 300, 350, 400, 420, 450, 500, 550, 650, 850, 985, 1000, 1250, 1450, 1600];
    var fr_values = [0, 190, 255, 340, 350, 400, 450, 470, 490, 500, 550, 725, 775];
    var mp_values = [0, 50, 87, 112];

    // For Available values
    function availables(filter) {
        $.each(filter['horsepower'], function( index, value ) { 
            $('.range-inner--horsepower .ruler-value [data-value="'+value+'"]').addClass('avl');
        });
        $.each(filter['flow_rate'], function( index, value ) { 
            $('.range-inner--flow-rate .ruler-value [data-value="'+value+'"]').addClass('avl');
        });
        $.each(filter['max_pressure'], function( index, value ) { 
            $('.range-inner--max-pressure .ruler-value [data-value="'+value+'"]').addClass('avl');
        });
        if(filter['fuel_type'][0] == "GAS") {
            $('.range-inner--fuel-type .ruler-value [data-value="GAS"]').addClass('avl');
        }
        else if(filter['fuel_type'][0] == "FLEX") {
            $('.range-inner--fuel-type .ruler-value [data-value="FLEX"]').addClass('avl');
        }
    }

    // Horsepower
    $('#horsepower').on('change', function (e) {
        e.stopPropagation();
        var min = e.target.min,
            max = e.target.max,
            val = e.target.value;
            ruler = $("horsepower").find('.ruler-value span');

        // set backgroud for slider
        $(e.target).css({
            'backgroundSize': (val - min) * 100 / (max - min) + '% 100%'
        });

        $('.range-inner--horsepower').find('.ruler-value span').removeClass('active');
        $('.range-inner--horsepower').find('.ruler-value span:nth-child(' + (val) + ')').addClass('active');
        $(".range-inner--horsepower").find(".value").attr("value", hp_values[this.value]);

        $// Rotate Meter
        $('.horsepower-meter').find('.counter, .counter-ie').text(hp_values[this.value]);
        var START = 0;
        var delta = 0.0818;
        var deg = START + hp_values[this.value] * delta;
        setTimeout(function(){
            $('.horsepower-meter').find('.arrow').css({
                "-webkit-transform": "rotate(" + deg + "deg)",
                "-o-transform": 'rotate(' + deg + 'deg)',
                "-moz-transform": 'rotate(' + deg + 'deg)',
                "transform": 'rotate(' + deg + 'deg)',
            });
        },1000);

        setTimeout(function(){
            $('.ruler-value span').removeClass('avl');
            var filter = checkAvailableFilters($('#horsepower_value').val(),"","","");
            availables(filter);
        },100);

        // Fetch results
        searchFilterPumps();
    });

     //Flowrate
     $('#flow-rate').on('change', function (e) {
        e.stopPropagation();
        var min = e.target.min,
            max = e.target.max,
            val = e.target.value;
            ruler = $("flow-rate").find('.ruler-value span');

        var currentVal = this.value;
        
        // restrict click on unavailable values
        if($('.range-inner--flow-rate').find('.ruler-value span:nth-child(' + (currentVal) + ')').hasClass('avl')) {
            currentVal = this.value;
        } else if(!$('.range-inner--flow-rate').find('.ruler-value span').hasClass('avl')) {
            currentVal = this.value;
        } else {
            var closest_avl = "";
            if($('.range-inner--flow-rate').find('.ruler-value span:nth-child(' + (currentVal) + ')').prevAll(".avl").length > 0) {
                closest_avl = $('.range-inner--flow-rate').find('.ruler-value span:nth-child(' + (currentVal) + ')').prevAll(".avl:first").data("count"); 
            } else {
                closest_avl = $('.range-inner--flow-rate').find('.ruler-value span:nth-child(' + (currentVal) + ')').nextAll(".avl:first").data("count");
            }
            currentVal = closest_avl;
        }

        // set backgroud for slider
        $(e.target).css({
            'backgroundSize': (currentVal - min) * 100 / (max - min) + '% 100%'
        });

        $('#flow-rate').val(currentVal);

        $('.range-inner--flow-rate').find('.ruler-value span').removeClass('active');
        $('.range-inner--flow-rate').find('.ruler-value span:nth-child(' + (currentVal) + ')').addClass('active');        
        $(".range-inner--flow-rate").find(".value").attr("value", fr_values[currentVal]);
        $(".range-inner--flow-rate").find(".value, .range-counter").val(fr_values[currentVal]).text((fr_values[currentVal]));

         // Rotate Meter
        $('.flow-rate-meter').find('.counter, .counter-ie').text(fr_values[currentVal]);
        var START = 0;
        var delta = 0.218;
        var deg = START + fr_values[currentVal] * delta;
        setTimeout(function(){
            $('.flow-rate-meter').find('.arrow').css({
                "-webkit-transform": 'rotate(' + deg + 'deg)',
                "-o-transform": 'rotate(' + deg + 'deg)',
                "-moz-transform": 'rotate(' + deg + 'deg)',
                "transform": 'rotate(' + deg + 'deg)',
            });
        }, 1000);

        setTimeout(function(){
            $('.ruler-value span').removeClass('avl');
            var filter = checkAvailableFilters("",$('#flow-rate_value').val(),"","");
            availables(filter);
        },100);

        // Fetch results
        searchFilterPumps();
     });

     //Max Pressure
    $('#max-pressure').on('change', function (e) {
        e.stopPropagation();
        var min = e.target.min,
            max = e.target.max,
            val = e.target.value;
            ruler = $("max-pressure").find('.ruler-value span');

        var currentVal = this.value;

        // restrict click on unavailable values
        if($('.range-inner--max-pressure').find('.ruler-value span:nth-child(' + (currentVal) + ')').hasClass('avl')) {
            currentVal = this.value;
        } else if(!$('.range-inner--max-pressure').find('.ruler-value span').hasClass('avl')) {
            currentVal = this.value;
        } else {
            var closest_avl = "";
            if($('.range-inner--max-pressure').find('.ruler-value span:nth-child(' + (currentVal) + ')').prevAll(".avl").length > 0) {
                closest_avl = $('.range-inner--max-pressure').find('.ruler-value span:nth-child(' + (currentVal) + ')').prevAll(".avl:first").data("count"); 
            } else {
                closest_avl = $('.range-inner--max-pressure').find('.ruler-value span:nth-child(' + (currentVal) + ')').nextAll(".avl:first").data("count");
            }
            currentVal = closest_avl;
        }

         // set backgroud for slider
         $(e.target).css({
            'backgroundSize': (currentVal - min) * 100 / (max - min) + '% 100%'
        });

        $('#max-pressure').val(currentVal);

        $('.range-inner--max-pressure').find('.ruler-value span').removeClass('active');
        $('.range-inner--max-pressure').find('.ruler-value span:nth-child(' + (currentVal) + ')').addClass('active');
        $(".range-inner--max-pressure").find(".value").attr("value", mp_values[currentVal]);
        $(".range-inner--max-pressure").find(".value, .range-counter").val(mp_values[currentVal]).text((mp_values[currentVal]));

        // Rotate Meter
        $('.max-pressure-meter').find('.counter, .counter-ie').text(mp_values[this.value]);
        var START = 0;
        var delta = 1.15;
        var deg = START + mp_values[this.value] * delta;
        setTimeout(function(){
            $('.max-pressure-meter').find('.arrow').css({
                "-webkit-transform": 'rotate(' + deg + 'deg)',
                "-o-transform": 'rotate(' + deg + 'deg)',
                "-moz-transform": 'rotate(' + deg + 'deg)',
                "transform": 'rotate(' + deg + 'deg)',
            });
        }, 1000);

        setTimeout(function(){
            $('.ruler-value span').removeClass('avl');
            var filter = checkAvailableFilters("","",$('#max-pressure_value').val(),"");
            availables(filter);
        },100);

        // Fetch results
        searchFilterPumps();
    });

    // Fuel Type
    $('#fuel-type').on('change', function (e) {
        e.stopPropagation();
        var min = e.target.min,
            max = e.target.max,
            val = e.target.value;

        $(e.target).css({
            'backgroundSize': (val - min) * 100 / (max - min) + '% 100%'
        });

        $('.range-inner--fuel-type').find('.ruler-value span').removeClass('active');

        var fltype = "";
        if (val == -1) {
            fltype = "GAS";
            $(".range-inner--fuel-type .toggle-wrapper").removeClass("max").addClass("min");
            $('.range-inner--fuel-type').find('.ruler-value span:first-child').addClass('active');
        } 
        else if (val == 1) {
            fltype = "FLEX";
            $(".range-inner--fuel-type .toggle-wrapper").removeClass("min").addClass("max");
            $('.range-inner--fuel-type').find('.ruler-value span:last-child').addClass('active');
        }
        else if (val == 0) {
            fltype = "";
            $(".range-inner--fuel-type .toggle-wrapper").removeClass("min").removeClass("max");
        }        

        $(".range-inner--fuel-type").find(".value").attr("value", fltype);
        $(".range-inner--fuel-type").find(".value").attr("value", fltype);
        $(".range-inner--fuel-type").find(".value, .range-counter").val(fltype).text(fltype);

        // Fetch results
        searchFilterPumps();
    });

     // Reset
     $("#reset").on("click", function (e) {
        e.preventDefault();
        $("input[type='range']").val(0).css({
            'backgroundSize': 0
        });
        $(".arrow").css({
            "transform": 'rotate(' + 0 + 'deg)'
        });
        $(".horsepower-meter .counter").attr('x', '-5%');
        $(".flow-rate-meter .counter, .max-pressure-meter .counter").attr('x', '-6%');
        $(".midPoints, .mainPoints").find('path').removeClass('active');
        $(".ruler-value").removeClass('active');
        $(".ruler-value span").removeClass('avl active');
        $(".counter, .counter-ie, .value, .range-counter").text(0).val(0);
        $(".toggle-wrapper, .fuel-meter").removeClass("min max");
    });
});