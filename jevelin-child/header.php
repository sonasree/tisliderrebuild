<?php
/**
 * Header
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">

	<?php if( jevelin_option('responsive_layout') ) : ?>
		<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php endif; ?>

	<?php
	/* Callback for older WordPress versions */
	if( !function_exists( 'has_site_icon' ) || ! has_site_icon() ) : ?>
		<?php if( jevelin_option_image('favicon') ) : ?>
			<link rel="icon" type="image/png" href="<?php echo jevelin_option_image('favicon'); ?>">
		<?php endif; ?>
	<?php endif; ?>

	<?php wp_head(); ?>
<!-- Global site tag (gtag.js) - Google AdWords: 854114979 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-854114979"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-854114979');
</script>


<script type="text/javascript">
(function(a,e,c,f,g,h,b,d){var k={ak:"854114979",cl:"eTzmCPzQ9XIQo4WjlwM",autoreplace:"248-296-8000"};a[c]=a[c]||function(){(a[c].q=a[c].q||[]).push(arguments)};a[g]||(a[g]=k.ak);b=e.createElement(h);b.async=1;b.src="//www.gstatic.com/wcm/loader.js";d=e.getElementsByTagName(h)[0];d.parentNode.insertBefore(b,d);a[f]=function(b,d,e){a[c](2,b,k,d,null,new Date,e)};a[f]()})(window,document,"_googWcmImpl","_googWcmGet","_googWcmAk","script");
</script>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PGJF654');</script>
<!-- End Google Tag Manager -->


</head>
<body <?php body_class(); ?>>
	<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PGJF654"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<?php
	/* Get Titlebar options */
	$titlebar_style_val = jevelin_post_option( jevelin_page_id(), 'header_style' );
	$titlebar_style_val2 = ( isset( $titlebar_style_val['header_style'] ) ) ? esc_attr($titlebar_style_val['header_style']) : 'default';

	/* Inlcude page loader HTML */
	get_template_part('inc/templates/page_loader' );

	/* Header popup search */
	if( jevelin_option( 'header_search_style', 'style1' ) != 'style1' ) :
		get_template_part('inc/headers/header-search');
	endif;
?>

	<?php
		if( jevelin_header_right() ) :
			/* Inlcude right header HTML */
			get_template_part('inc/headers/header-right-1' );
		endif;
	?>

	<div id="page-container" class="<?php echo jevelin_header_page_container(); ?>">
		<?php
			/* Inlcude page notice HTML */
			get_template_part('inc/templates/notice' );

			/* Inlcude breadcrumbs HTML, if bottom titlebar style */
			if( $titlebar_style_val2 == 'bottom_titlebar' ) :
				get_template_part('inc/templates/titlebar' );
			endif;
		?>


		<?php if( jevelin_post_option( jevelin_page_id(), 'header', 'on' ) != 'off' ) : ?>
			<header class="primary-mobile<?php echo jevelin_header_mobile_style(); ?>">
				<?php /* Inlcude mobile header */
					get_template_part('inc/headers/header-mobile' );
				?>
			</header>
			<header class="primary-desktop<?php echo jevelin_header_desktop_style(); ?>">
				<?php /* Inlcude desktop header */
					get_template_part('inc/headers/header-'.jevelin_header_layout() );
				?>
			</header>
		<?php endif; ?>


		<?php
			/* Inlcude breadcrumbs HTML, if not bottom titlebar style */
			if( $titlebar_style_val2 != 'bottom_titlebar' ) :
				get_template_part('inc/templates/titlebar' );
			endif;

			$class = '';
			if( jevelin_post_option( jevelin_page_id(), 'page_layout', 'default' ) != 'full' ) {
				$class = ' sh-page-layout-default';
			} else if( jevelin_post_option( jevelin_page_id(), 'page_layout', 'default' ) == 'full' ) {
				$class = ' sh-page-layout-full';
			}

		?>


			<div id="wrapper">
				<div class="content-container<?php echo esc_attr( $class ); ?>"">
				<?php if( jevelin_post_option( jevelin_page_id(), 'page_layout', 'default' ) != 'full' ) : ?>
					<div class="container entry-content">
				<?php endif; ?>


				<?php
					/* Inlcude white borders option HTML */
					get_template_part('inc/templates/white_borders' );
				?>
